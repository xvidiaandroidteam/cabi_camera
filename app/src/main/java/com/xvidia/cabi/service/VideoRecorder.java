package com.xvidia.cabi.service;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnInfoListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.xvidia.cabi.R;
import com.xvidia.cabi.backgroundService.UploadOfflineVideoTask;
import com.xvidia.cabi.data.UploadData;
import com.xvidia.cabi.utils.AppUtil;
import com.xvidia.cabi.utils.Logger;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

//*******************************************************
//*******************************************************
// CamaraView
//*******************************************************
//*******************************************************
public class VideoRecorder extends Activity implements SurfaceHolder.Callback {
	MediaRecorder mRecorder;
	SurfaceHolder mHolder;
	boolean mRecording = false;
	boolean mStop = false;
	boolean mPrepared = false;
	public static boolean newDownload = false;
	String filePath = "";
	static int VIDEO_TIME = 30 * 1000;
	final int TIMER_TIME = 10000;
	String android_id;
	static boolean WerbRtcApp;
	static Handler handler = null;
	static private Runnable runnableBackground;
	int videoWidth , videoHeight; 
//	TextToSpeech ttsObj;
    private BluetoothAdapter myBluetoothAdapter;
	// private PowerManager.WakeLock mWakeLock;

	final static public String FOLDER_PATH = "/xvidia/Surveillance/";

	// private Timer mStopTimer;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		WerbRtcApp = true;
		videoWidth = 0;
		videoHeight = 0; 
		mRecorder = new MediaRecorder();
		initRecorder();
		setContentView(R.layout.camera);
		Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this,VideoRecorder.class));
		SurfaceView cameraView = (SurfaceView) findViewById(R.id.surface_camera);
		mHolder = cameraView.getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		
//		ttsObj=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
//     	   @Override
		
//     	   public void onInit(int status) {
//					if (status != TextToSpeech.ERROR) {
//				        ttsObj.setLanguage(Locale.US);
//					}
//     	   }
//     	}
//     	);
		//
		// cameraView.setClickable(true);
		// cameraView.setOnClickListener(this);

		// PowerManager pm = (PowerManager)
		// getSystemService(Context.POWER_SERVICE);
		// mWakeLock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP,
		// "Ping");
		// mWakeLock.acquire();

//		new Handler().postDelayed(new Runnable() {
//
//			public void run() {
//				startRecording();
//
//			}
//		}, 2000);
		
		if(savedInstanceState == null){
		
		setAndroidId();
//			if(AppUtil.checkNetwrk() &&!register){
//				String username = LogData.getInstance().getUserName(this);
//	    		sendPostRequestToServer(new ServiceURLManager().getAPIGetAddCameraRequest(username, android_id));
//			}else{
				new Handler().postDelayed(new Runnable() {
	
					public void run() {
//						if(!WerbRtcApp)
//						startRecording();
						Intent intent = new Intent(VideoRecorder.this, PublishLiveStreamActivity.class);
						startActivity(intent);
//						startService(new Intent(VideoRecorder.this,ObdGatewayService.class));
					}
				}, 1000);
//			}
//			new Handler().postDelayed(new Runnable() {
//				@Override
//				public void run() {
//					try {
//						Intent i = new Intent();
//						i.setComponent(new ComponentName("com.xvidia.cabi", "com.xvidia.obd.reader.io.ObdGatewayService"));
//						ComponentName c = startService(i);
//					}catch(Exception e){
//
//					}
////					Log.e(TAG, "ComponentName restart. -> " + c);
//				}
//			},1000);
//		 myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//	        if(myBluetoothAdapter != null){
//	        	if (!myBluetoothAdapter.isEnabled()) {
//	          	  myBluetoothAdapter.enable();
////					startService(new Intent(VideoRecorder.this,ObdGatewayService.class));
////	        	}else{
////	        		myBluetoothAdapter.disable();
//	        		new Handler().postDelayed(new Runnable() {
//	        			
//						public void run() {
////							myBluetoothAdapter.enable();
//							if(!LogData.getInstance().getVideoRestart(MyApplication.getAppContext()))
//								startService(new Intent(VideoRecorder.this,ObdGatewayService.class));
//							
//
////							if(WerbRtcApp)
////								launchCameraApplication();
//						}
//					}, 2500);
//	        	}else{
////	        		myBluetoothAdapter.disable();
//	        		new Handler().postDelayed(new Runnable() {
//	        			
//						public void run() {
////							myBluetoothAdapter.enable();
//							if(!LogData.getInstance().getVideoRestart(MyApplication.getAppContext()))
//								startService(new Intent(VideoRecorder.this,ObdGatewayService.class));
//
////							if(WerbRtcApp)
////								launchCameraApplication();
//						}
//					}, 2500);
//	        	}
//	        }
		}
	}
	 private void launchCameraApplication(){
	    	try{
//	    		WerbRtcApp = false;
	    			Intent intent = getPackageManager().getLaunchIntentForPackage("com.xvidia.cabi.camera");
	    			startActivity( intent );
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	 }

	private void setAndroidId(){
		android_id  = ""+System.currentTimeMillis();
		Random r = new Random();
		int temp = r.nextInt(999999999) + 65;
		android_id = android_id+temp;
		try{
			android_id = Secure.getString(getContentResolver(),Secure.ANDROID_ID);
		}
		catch (Exception e) {

		}
//		if(android_id.isEmpty()){
//			android_id = LogData.getInstance().getUserName(this);
//		}
	}


	@Override
	public void onBackPressed() {
//		try {
//			if (mRecording) {
//				mRecorder.stop();
//				mRecording = false;
//				mRecorder.release();
//			}
//		} catch (IllegalStateException e) {
//			Logger.Error("prepareRecorder_IllegalStateException", e);
//			// finish();
//		} catch (Exception e) {
//			Logger.Error("prepareRecorder", e);
//		}
	}
	@Override
	public void onPause() {

//        startActivity(getIntent().addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
//		restartRecorder();
		try {
			if (mRecording && mRecorder != null) {
				mRecorder.stop();
				mRecording = false;
				mRecorder.release();
			}
		} catch (IllegalStateException e) {
			Logger.Error("prepareRecorder_IllegalStateException", e);
			// finish();
		} catch (Exception e) {
			Logger.Error("prepareRecorder", e);
		}
//        LogData.getInstance().setVideoRestart(true, MyApplication.getAppContext());
//        
//		startTimer(TIMER_TIME);
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		if(handler != null){
			 handler.removeCallbacks(runnableBackground);
			 handler = null;
			}
	}

	private void startTimer(int milSec){
		try{
		if(handler != null){
			 handler.removeCallbacks(runnableBackground);
			 handler = null;
			}
				runnableBackground = new Runnable() {
			    	   @Override
			    	   public void run() {
//			    			if(! ClientConnectionConfig._HARDWAREKEY.equals("") ){	    				
//			    				if(AppState.BACKPRESSED_SIGNAGE_SCREEN){
			    					Intent intent = getIntent();   
			    					finish();
			    					startActivity(intent);	
//			    				}
//			    			}
			    			if(handler != null){
								 handler.removeCallbacks(runnableBackground);
								 handler = null;
								}
			    	   }
			    	};
			handler =  new Handler();
			handler.postDelayed(runnableBackground, milSec);
		}catch(Exception e){
			
		}
	}
	/**
	 * Shows keeping the access keys returned from Trusted Authenticator in a
	 * local store, rather than storing user name & password, and
	 * re-authenticating each time (which is not to be done, ever).
	 */
	// private void storeKeys(String key, String secret) {
	// // Save the access key for later
	// SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
	// Editor edit = prefs.edit();
	// edit.putString(ACCESS_KEY_NAME, key);
	// edit.putString(ACCESS_SECRET_NAME, secret);
	// edit.commit();
	// }

	// private void clearKeys() {
	// SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
	// Editor edit = prefs.edit();
	// edit.clear();
	// edit.commit();
	// }

	// private void checkAppKeySetup() {
	//
	//
	// // Check if the app has set up its manifest properly.
	// Intent testIntent = new Intent(Intent.ACTION_VIEW);
	// String scheme = "db-" + APP_KEY;
	// String uri = scheme + "://" + AuthActivity.AUTH_VERSION + "/test";
	// testIntent.setData(Uri.parse(uri));
	// PackageManager pm = getPackageManager();
	// if (0 == pm.queryIntentActivities(testIntent, 0).size()) {
	// Toast.makeText(this, "URL scheme in your app's " +
	// "manifest is not set up correctly. You should have a " +
	// "com.dropbox.client2.android.AuthActivity with the " +
	// "scheme: " + scheme, Toast.LENGTH_SHORT).show();
	// finish();
	// }
	// }

	// private void logOut() {
	// // Remove credentials from the session
	// mApi.getSession().unlink();
	//
	// // Clear our stored keys
	// clearKeys();
	// // Change UI state to display logged out version
	// setLoggedIn(false);
	// }

	// private void setLoggedIn(boolean loggedIn) {
	// mLoggedIn = loggedIn;
	// }

	@SuppressWarnings("deprecation")
	private Size getBestPictureSize() {
		Camera camera = Camera.open();
		List<Size> camSizeList = null;
		Camera.Size result = null;
		 if(camera != null){
	    if (camera.getParameters().getSupportedVideoSizes() != null) {
	    	camSizeList = camera.getParameters().getSupportedVideoSizes();
	    } else {
	        // Video sizes may be null, which indicates that all the supported 
	        // preview sizes are supported for video recording.
	    	camSizeList = camera.getParameters().getSupportedPreviewSizes();
	    }
		try { 
			for (Camera.Size size : camSizeList) {
				Log.d("Video Sizess", "result PICTURE SIZE: width " + size.width
						+ " height: " + size.height);
				if (result == null) {
					result = size;
				} else {
					int resultArea = result.width * result.height;
					int newArea = size.width * size.height;

					if (newArea > resultArea) {
						result = size;
					}
				}
				ratio(size.width, size.height);
			}			
			
		} catch (Exception e) {

		}
		 }
		 if(camera != null){
			 camera.release();
		 }
		return (result);
	}
	int gcd(int p, int q) {
	    if (q == 0) return p;
	    else return gcd(q, p % q);
	}

	void ratio(int a, int b) {
	   final int gcd = gcd(a,b);
	   int widthRatio = a/gcd;
	   int heightRatio = b/gcd;
	   
	
			if (widthRatio == 16 && heightRatio == 9) {
				if (videoWidth < a) {
					videoWidth = a;
					videoHeight = b;
				}
				Log.d("Video Sizess", "camera ratio SIZE: width " + videoWidth
						+ " height: " + videoHeight);
			}
	}	
	private void initRecorder() {
		if(mRecorder != null){
//			Size pictureSize=getBestPictureSize();
//	      	if(videoWidth == 0 && pictureSize != null){
//	      		videoWidth = pictureSize.width;
//	      		videoHeight = pictureSize.height;
//			}
//	        if (videoHeight !=0 && videoWidth != 0) {
			mRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
			mRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
			mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
			mRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
			mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
			mRecorder.setMaxDuration(VIDEO_TIME);
	
			mRecorder.setVideoFrameRate(15);
			mRecorder.setAudioSamplingRate(48000);
			 mRecorder.setVideoEncodingBitRate(56000);
//			mRecorder.setVideoSize(videoWidth, videoHeight);
//			showToast(" setVideoSize " +videoWidth + "height " +videoHeight);
	//		mRecorder.setVideoFrameRate(10);
	
			// File mediaStorageDir = new File("/sdcard/Surveillance/");
			// if ( !mediaStorageDir.exists() ) {
			// if ( !mediaStorageDir.mkdirs() ){
			// Logger.Debug("failed to create directory");
			// }
			// }
			filePath = getVideoPath();// "/sdcard/Surveillance/" + "VID_"+ new
										// SimpleDateFormat("yyyyMMdd_HHmmss").format(new
										// Date()) + ".mp4";
			// mFiles.add(filePath);
			mRecorder.setOutputFile(filePath);
			mRecorder.setOnInfoListener(new OnInfoListener() {
	
				public void onInfo(MediaRecorder mr, int what, int extra) {
					if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
						try {
							showToast(" restartRecorder " +filePath);
							restartRecorder();
						} catch (Exception e) {
							showToast(" restartRecorder error " +filePath + e.getMessage());
						}
					}
	
				}
			});
			
//			mRecorder.setOnErrorListener(new OnErrorListener() {
//				
//				@Override
//				public void onError(MediaRecorder mr, int what, int extra) {
//					try {	
//						showToast(" restartRecorder error " +filePath );
//					
//						restartRecorder();
//					} catch (Exception e) {	
//						showToast(" restartRecorder error " +filePath + e.getMessage());
//					
//					}
//				}
//			});
//		}
		}
	}

	private String getVideoPath() {
		// String exernal = Environment.getExternalStorageState();
		String filePath = Environment.getExternalStorageDirectory().toString()
				+ FOLDER_PATH;
		File mediaStorageDir = new File(filePath);
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				// Logger.Debug("failed to create directory");
			}
		}
		filePath = filePath
				+ "VID_"
				+ new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
						.format(new Date()) + ".mp4";
		return filePath;
	}

	private void prepareRecorder() {
		Logger.Debug("About to prepare");
		if(mRecorder != null){
			mRecorder.setPreviewDisplay(mHolder.getSurface());
	
			try {
				mRecorder.prepare();
				mPrepared = true;
				Logger.Debug("right after prepare");
			} catch (IllegalStateException e) {
				Logger.Error("prepareRecorder_IllegalStateException", e);
				// finish();
			} catch (IOException e) {
				Logger.Error("prepareRecorder_IOException", e);
				// finish();
			} catch (Exception e) {
				Logger.Error("prepareRecorder", e);
			}
			Logger.Debug("after prepare");
		}
	}

	void startRecording() {
		try {
			if (mRecording) {
				Logger.Debug("Stopping");
				mRecorder.reset();
				mPrepared = false;
				mRecording = false;
				this.initRecorder();
				this.prepareRecorder();

				showToast("stopped");
				// mStopTimer.cancel();
			} else {
				// prepareRecorder();
				mRecorder.start();
				mRecording = true;
				showToast("started");
			}
			
		} catch (Exception e) {
			Logger.Error("onClick", e);
		}
	}

	public void restartRecorder() {
//		Logger.Debug("'bout to restart recorder");

		mRecorder.reset();
		mPrepared = false;
		mRecording = false;
		File fileBody = new File(filePath);
		UploadData obj = new UploadData();
		obj.setFilePath(filePath);
		obj.setName(fileBody.getName());
		obj.setUploadingState("false");
		if(AppUtil.checkNetwrk()){
//			DataCacheManager.getInstance().savePendingUploadData(obj);
			if(!newDownload){
//				showToast("upload "+filePath );
				new UploadOfflineVideoTask(VideoRecorder.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
			}
//		}else{
//			DataCacheManager.getInstance().saveOfflineUploadData(obj);	
		}
		initRecorder();
		prepareRecorder();
		// Start uploading the last file
		mRecorder.start();
		mRecording = true;

	}

	public void surfaceCreated(SurfaceHolder holder) {
		if (!mPrepared) {
			prepareRecorder();
		}
	}
	 private void showToast(String msg) {
	        Toast error = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
	        error.show();
	    }
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
//		Logger.Debug("surfaceChanged called");
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		try{
		if (mRecording && mRecorder != null) {
			mRecorder.stop();
			mRecording = false;
			 mRecorder.release();
			 mRecorder = null;
			 mPrepared = false;
		}
//		mRecorder.reset();
		}catch(IllegalStateException e){
			e.printStackTrace();
		}catch(Exception e){e.printStackTrace();}
	}

}