package com.xvidia.cabi.service;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xvidia.cabi.R;
import com.xvidia.cabi.data.CameraData;
import com.xvidia.cabi.data.DeviceData;
import com.xvidia.cabi.data.LogData;
import com.xvidia.cabi.network.IAPIConstants;
import com.xvidia.cabi.network.RequestData;
import com.xvidia.cabi.network.ServiceURL;
import com.xvidia.cabi.network.ServiceURLManager;
import com.xvidia.cabi.network.VolleySingleton;
import com.xvidia.obd.reader.io.ObdGatewayService;

import org.json.JSONObject;

import java.io.IOException;


public class StartActivity extends Activity {
    private String android_id;

	private TextView messageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
		messageView = (TextView)findViewById(R.id.text) ;
        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this,StartActivity.class));
        android_id = Secure.getString(getApplicationContext().getContentResolver(),
				Secure.ANDROID_ID);
        Log.i("onMessage", "onMessage----->>>"+android_id);
        showToast("android_id "+android_id );
		startService(new Intent(this,LocationService.class));
        if(LogData.getInstance().getDeviceId(MyApplication.getAppContext())== null){
			RequestData obj = new RequestData();
			obj.setDeviceId(android_id);
			messageView.setText(getString(R.string.updating));
			sendPostRequest(obj, IAPIConstants.API_KEY_DEVICE_INIT);
        }else if(LogData.getInstance().getAssetIdKey(MyApplication.getAppContext())== null){
        	if(android_id.equals(LogData.getInstance().getDeviceId(MyApplication.getAppContext()))){
        		android_id = LogData.getInstance().getDeviceId(MyApplication.getAppContext());

				RequestData obj = new RequestData();
				obj.setDeviceId(android_id);
				obj.setAssetId(android_id);

				messageView.setText(getString(R.string.register));
				sendPostRequest(obj, IAPIConstants.API_KEY_PRE_REG_DEMO);
        	}
        }else{
        	String temp = LogData.getInstance().getDeviceId(MyApplication.getAppContext());

            Log.e("onMessage", "onMessage----->>>"+temp);
        	if(android_id.equals(LogData.getInstance().getDeviceId(MyApplication.getAppContext()))){
        		android_id = LogData.getInstance().getDeviceId(MyApplication.getAppContext());
				String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REGISTER_DEVICE);
				url = url+android_id;
				messageView.setText(getString(R.string.car_info_wait));
				sendGetRequest(url);
//            	sendGetRequestToServer(new ServiceURLManager().getAPIGetRegisterRequest(android_id));
            	}
        }
    }

    

	/**
	 * The nework connection status is returned
	 * @return true if network is connected else false
	 * @since version 1.0
	 */
	boolean checkNetwrk(){
		boolean nwFlag = false;
		try{
			ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isConnected()) {
				nwFlag = true;
			}

		}catch (Exception e) {
		}

		return nwFlag;
	}
	private void sendPostRequest(RequestData requestData, int api){
		if(requestData != null){

			if(checkNetwrk()){
				final String url = ServiceURLManager.getInstance().getUrl(api);
				ObjectMapper mapper = new ObjectMapper();
				String jsonRequestString = null;
				try {
					jsonRequestString = mapper.writeValueAsString(requestData);
					Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
				} catch (IOException e) {
				}

				JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonRequestString, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {

						if(url.contains(ServiceURL.API_KEY_DEVICE_INIT)){
							ObjectMapper mapper = new ObjectMapper();
							CameraData cameraObj = null;
							try {
								cameraObj = mapper.readValue(response.toString(), CameraData.class);
							} catch (IOException e) {
								e.printStackTrace();
							}
							if(cameraObj !=null) {
								String deviceId = cameraObj.getDeviceId();

								if (deviceId.equalsIgnoreCase(android_id)) {
									LogData.getInstance().setDeviceIdKey(deviceId, MyApplication.getAppContext());
									RequestData obj = new RequestData();
									obj.setDeviceId(android_id);
									obj.setAssetId(android_id);

									messageView.setText(getString(R.string.register));
									sendPostRequest(obj, IAPIConstants.API_KEY_PRE_REG_DEMO);
//			        	sendPostRequestToServer(new ServiceURLManager().getAPIGetPreRegDevice(android_id, android_id));
								}
							}else{
								RequestData obj = new RequestData();
								obj.setDeviceId(android_id);
								messageView.setText(getString(R.string.updating));
								sendPostRequest(obj, IAPIConstants.API_KEY_DEVICE_INIT);
							}
						}else if(url.contains(ServiceURL.API_KEY_PRE_REG_DEMO)){
							ObjectMapper mapper = new ObjectMapper();
							CameraData cameraObj = null;
							try {
								cameraObj = mapper.readValue(response.toString(), CameraData.class);
							} catch (IOException e) {
								e.printStackTrace();
							}
							if(cameraObj !=null) {
								String assetId = cameraObj.getAssetId();
								if (assetId.equalsIgnoreCase(android_id)) {
									LogData.getInstance().setAssetIdKey(assetId, MyApplication.getAppContext());
									String url = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REGISTER_DEVICE);
									url = url + android_id;

									messageView.setText(getString(R.string.car_info_wait));
									sendGetRequest(url);
//			        	sendGetRequestToServer(new ServiceURLManager().getAPIGetRegisterRequest(android_id));
								}
							}else{
								RequestData obj = new RequestData();
								obj.setDeviceId(android_id);
								messageView.setText(getString(R.string.updating));
								sendPostRequest(obj, IAPIConstants.API_KEY_DEVICE_INIT);
							}
//						}else if(url.contains(ServiceURL.API_KEY_REGISTER_DEVICE)){
//							ObjectMapper mapper = new ObjectMapper();
//							Car car = null;
//							try {
//								car = mapper.readValue(response.toString(), Car.class);
//							} catch (IOException e) {
//								e.printStackTrace();
//							}
//							String carRegNo = car.getRegistrationNumber();
//							LogData.getInstance().setCarRegNoKey(carRegNo, MyApplication.getAppContext());
//							launchCameraApplication();
						}
						}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						if(error != null) {
							NetworkResponse networkResponse = error.networkResponse;
							if (networkResponse != null) {
								onError(networkResponse.statusCode, url);
							}else{
								messageView.setText(getString(R.string.time_out_error));

							}
						}
					}
				});
				VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
			}
		}
	}

	private void sendGetRequest(final String url) {

		if (checkNetwrk()) {

			messageView.setText(getString(R.string.car_info_wait));
			JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
						ObjectMapper mapper = new ObjectMapper();
						DeviceData cameraObj = null;
						try {
							cameraObj = mapper.readValue(response.toString(), DeviceData.class);
						} catch (IOException e) {
							e.printStackTrace();
						}
						if(cameraObj!=null) {
							String carRegNo = cameraObj.getRegistrationNumber();
							LogData.getInstance().setCarRegNoKey(carRegNo, MyApplication.getAppContext());
							launchCameraApplication();
						}
				}
			}, new Response.ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					if(error != null) {
						NetworkResponse networkResponse = error.networkResponse;
						if (networkResponse != null) {
							onError(networkResponse.statusCode, url);
						}
					}
				}
			});
			VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
		}
	}
    private void showToast(String msg) {
        Toast error = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        error.show();
    }
    private void launchCameraApplication(){
    	try{
    		   Intent intent = new Intent(StartActivity.this, PublishLiveStreamActivity.class);
    		   finish();
    		   startActivity(intent);
//    		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}

    }



	public void onError(int code,String url) {
		
		if(url.contains(ServiceURL.API_KEY_DEVICE_INIT)){
//			launchCameraApplication();
			if(code == 409){
				LogData.getInstance().setDeviceIdKey(android_id,MyApplication.getAppContext());
				RequestData obj = new RequestData();
				obj.setDeviceId(android_id);
				obj.setAssetId(android_id);
				sendPostRequest(obj, IAPIConstants.API_KEY_PRE_REG_DEMO);
			}else if(code == 404){
				
			}else if(code == 500){
				
			}
			
		}else if(url.contains(ServiceURL.API_KEY_PRE_REG_DEMO)){
			if(code == 409){
				LogData.getInstance().setAssetIdKey(android_id,MyApplication.getAppContext());
				android_id = LogData.getInstance().getDeviceId(MyApplication.getAppContext());
				String urlnew = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REGISTER_DEVICE);
				urlnew = urlnew+android_id;
				startService(new Intent(this,LocationService.class));
			}else if(code == 404){
				
			}else if(code == 500){
				
			}
//			sendGetRequestToServer(new ServiceURLManager().getAPIGetRegisterRequest(android_id));
		}else if(url.contains(ServiceURL.API_KEY_REGISTER_DEVICE)){
			if(code == 409){
				
			}else if(code == 404){
				
			}else if(code == 500){
				
			}
			new Handler().postDelayed(new Runnable() {
				
				@Override
				public void run() {

					android_id = LogData.getInstance().getDeviceId(MyApplication.getAppContext());
					String urlnew = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_REGISTER_DEVICE);
					urlnew = urlnew+android_id;
					sendGetRequest(urlnew);
				}
			}, 60*1000);
			
		}
		
	}


    
   
}
