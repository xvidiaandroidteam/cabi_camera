package com.xvidia.cabi.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xvidia.cabi.data.LocationData;
import com.xvidia.cabi.data.LogData;
import com.xvidia.cabi.network.IAPIConstants;
import com.xvidia.cabi.network.ServiceURLManager;
import com.xvidia.cabi.network.VolleySingleton;
import com.xvidia.obd.reader.io.AbstractGatewayService;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;

/**
 * This service is primarily responsible for establishing and maintaining a
 * permanent connection between the device where the application runs and a more
 * OBD Bluetooth interface.
 * <p/>
 * Secondarily, it will serve as a repository of ObdCommandJobs and at the same
 * time the application state-machine.
 */
public class LocationService extends Service implements LocationListener {

	private static final String TAG = LocationService.class.getName();
  /*
   * http://developer.android.com/reference/android/bluetooth/BluetoothDevice.html
   * #createRfcommSocketToServiceRecord(java.util.UUID)
   *
   * "Hint: If you are connecting to a Bluetooth serial board then try using the
   * well-known SPP UUID 00001101-0000-1000-8000-00805F9B34FB. However if you
   * are connecting to an Android peer then please generate your own unique
   * UUID."
   */

	private LocationManager locationManager = null;
	private Criteria criteria;
	private String provider;
	static int TIMER_TIME_2SEC = 15 * 1000;
	public static String android_id;
	static String trip_id = "tripId";
	static String regNo = "regNo";
	boolean obdConnectionSuccess = false;

	//  private BluetoothSocket sockFallback = null;
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "Starting service..");
		try {
			setAndroidId();
			initializeLocationManager();
		} catch (Exception e) {
			Log.e(
					TAG, "There was an error while establishing connection. -> "
							+ e.getMessage()
			);

			// in case of failure, stop this service.
			stopService();
//      throw new IOException();
		}

     /*
     * TODO clean
     *
     * Get more preferences
     */
//          boolean imperialUnits = prefs.getBoolean(ConfigActivity.IMPERIAL_UNITS_KEY,
//                  false);
//          ArrayList<ObdCommand> cmds = ConfigActivity.getObdCommands(prefs);
		return START_STICKY;
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}


	/**
	 * Stop OBD connection and queue processing.
	 */
	public void stopService() {
		if(locationManager!=null)
			locationManager.removeUpdates(this);
		stopSelf();
	}


	public class ObdGatewayServiceBinder extends Binder {
		public LocationService getService() {
			return LocationService.this;
		}
	}



	private void initializeLocationManager() {
		// Get the location manager
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		// Define the criteria how to select the location provider
		criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_COARSE);    //default

		criteria.setCostAllowed(false);
		// get the best provider depending on the criteria
		provider = locationManager.getBestProvider(criteria, false);

		// the last known location of this provider
		Location location = locationManager.getLastKnownLocation(provider);


		if (location != null) {
			onLocationChanged(location);
		} else {
			// leads to the settings because there is no last known location
//		  Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//		  startActivity(intent);
		}
		locationManager.requestLocationUpdates(provider, TIMER_TIME_2SEC, 1, this);
		// location updates: at least 1 meter and 200millsecs change
	}


	@Override
	public void onLocationChanged(Location location) {
		float lat = (float) location.getLatitude();
		float longitude = (float) location.getLongitude();
		float altitude = (float) location.getAltitude();
		float speed = location.getSpeed();
		Date nw = new Date();
		long time = nw.getTime();


		Log.i("onLocation Service", "onLocationChanged " + speed);

		sendLocationRequest(time, lat, longitude, altitude, speed);

	}

	@Override
	public void onProviderDisabled(String arg0) {

	}

	@Override
	public void onProviderEnabled(String arg0) {

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

	}


	/**
	 * Sends Location data
	 *
	 * @param timeStamp
	 * @param latitude
	 * @param longitude
	 * @param altitude
	 * @param speed
	 */
	public void sendLocationRequest(long timeStamp, float latitude, float longitude, float altitude, float speed) {
		if (android_id == null) {
			setAndroidId();
		}else if(android_id.isEmpty()){
			setAndroidId();
		}
		JSONObject jsonRequest = null;
		ObjectMapper mapper = new ObjectMapper();
		String jsonRequestString = null;
		LocationData locationData = new LocationData();
		locationData.setSensorId("GPS");
		locationData.setId(""+timeStamp);
		locationData.setDeviceId(android_id);
		locationData.setCarRegNo(regNo);
		locationData.setTripId(trip_id);
		locationData.setLat(latitude);
		locationData.setLongitude(longitude);
		locationData.setAltitude(altitude);
		locationData.setSpeed(speed);
		locationData.setTimeStamp(timeStamp);
		try {
			jsonRequestString = mapper.writeValueAsString(locationData);
			Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
		} catch (IOException e) {
		}

		sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_LOCATION);
	}

	/**
	 * Checks internet connectivity
	 *
	 * @return boolean true/false
	 */

	boolean checkNetwrk() {
		boolean nwFlag = false;
		try {
			ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isConnected()) {
				nwFlag = true;
			}

		} catch (Exception e) {
		}

		return nwFlag;
	}

	/**
	 * Sends http request to server
	 *
	 * @param jsonRequest
	 * @param api
	 */
	private void sendPostRequest(String jsonRequest, int api) {
		if (jsonRequest != null) {

			if (checkNetwrk()) {
				final String url = ServiceURLManager.getInstance().getUrl(api);

				JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						try {
							Log.i("Volley JSONObject ", response.toString());
						} catch (Exception e) {

						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						try {
							if (error != null) {
								NetworkResponse networkResponse = error.networkResponse;
								Log.i("Volley error service ", error.getMessage());
							}
						}catch (Exception e){

						}
					}
				});
				VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
			}
		}
	}

	private void setAndroidId() {
		android_id = LogData.getInstance().getDeviceId(MyApplication.getAppContext());//Secure.getString(getApplicationContext().getContentResolver(),Secure.ANDROID_ID);
//	android_id = "Cam1";

	}

}