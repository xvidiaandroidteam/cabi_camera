package com.xvidia.cabi.network;


import com.xvidia.cabi.R;
import com.xvidia.cabi.service.MyApplication;

public class ServiceURL {

//	final static String servicePath = "http://192.168.1.44:9000";
//	final static String jsonParam = "?json";
	
	public static final String API_KEY_DEVICE_INIT = "/deviceInit";
	public static final String API_KEY_REGISTER_DEVICE = "/getCar/";
	public static final String API_KEY_UPLOAD_VIDEO= "/upload";
	public static final String API_KEY_PRE_REG_DEMO= "/preRegister/device"; //TODO to be removed after demo
	public static final String API_KEY_VIEW_CAMERA = "/CamServer/cameraByUser/";
	public static final String API_KEY_OBD_COOLANT_TEMPERATURE = "/obdEngineCoolantTemp";
	public static final String API_KEY_OBD_SPEED = "/obdEngineSpeed";
	public static final String API_KEY_OBD_PRESSURE = "/obdFuelPressure";
	public static final String API_KEY_OBD_THROTTLE = "/obdThrottlePosition";
	public static final String API_KEY_OBD_RPM = "/obdRpm";
	public static final String API_KEY_OBD_FUELTYPE = "/obdFuelType";
	public static final String API_KEY_LOCATION = "/obd/gps";
	public static final String API_KEY_UPDATE_LIVE_API_KEY = "/addOpenTokSession";
	public static final String API_KEY_GET_PUBLISHER = MyApplication.getAppContext().getResources().getString(R.string.live_publisher_url);
	public static final String API_KEY_GET_SUBSCRIBER = MyApplication.getAppContext().getResources().getString(R.string.live_subscriber_url);

}
