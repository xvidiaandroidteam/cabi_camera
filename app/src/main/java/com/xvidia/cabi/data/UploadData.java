package com.xvidia.cabi.data;

public class UploadData {
	String fileName = "", filePath = "", uploadingState = "false";

	private String getValidatedString(String valStr) {
		if (valStr == null) {
			return "";
		} else {
			return valStr.trim();
		}
	}

	public String getName() {
		return fileName;
	}

	public void setName(String nameVal) {
		fileName = getValidatedString(nameVal);
	}
	

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String locVal) {
		filePath = getValidatedString(locVal);
	}
	
	public boolean getState() {
		boolean retVal = false;
		if(uploadingState.equalsIgnoreCase("true")){
			retVal = true;
		}
		return retVal;
	}

	public void setUploadingState(String state) {
		uploadingState = getValidatedString(state);
	}
	
}
