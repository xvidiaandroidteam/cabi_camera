package com.xvidia.cabi.data;
public class AcceleroGyroData {
	String sensorId = null;
	String cameraId = null;
	float axisX = 0;
	float axisY = 0;
	float axisZ = 0;
	long timeStamp = 0;

	public float getAxisZ() {
		return axisZ;
	}

	public void setAxisZ(float axisZ) {
		this.axisZ = axisZ;
	}

	public String getSensorId() {
		return sensorId;
	}

	public void setSensorId(String sensorId) {
		this.sensorId = sensorId;
	}

	public String getCameraId() {
		return cameraId;
	}

	public void setCameraId(String cameraId) {
		this.cameraId = cameraId;
	}

	public float getAxisX() {
		return axisX;
	}

	public void setAxisX(float axisX) {
		this.axisX = axisX;
	}

	public float getAxisY() {
		return axisY;
	}

	public void setAxisY(float axisY) {
		this.axisY = axisY;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	

	
	
}
