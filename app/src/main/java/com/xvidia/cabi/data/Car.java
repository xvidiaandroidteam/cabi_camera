package com.xvidia.cabi.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by vasu on 5/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Car {
    @JsonProperty("id")
    Long id;
    @JsonProperty("registrationNumber")
    String registrationNumber;

    @JsonProperty("ownerName")
    String ownerName;
    @JsonProperty("ownerMobileNo")
    String ownerMobileNo;
    @JsonProperty("ownerAddress")
    String ownerAddress;
    @JsonProperty("purchaseDate")
    String purchaseDate;
    @JsonProperty("model")
    String model;
    @JsonProperty("make")
    String make;
    @JsonProperty("isOnTrip")
    Boolean isOnTrip = false;
    @JsonProperty("apiKey")
    String apiKey;
    @JsonProperty("sessionId")
    String sessionId;
    @JsonProperty("token")
    String token;
    @JsonProperty("isActive")
    Boolean isActive = true;
    @JsonProperty("rating")
    Integer rating;
    @JsonProperty("remarks")
    String remarks;
    @JsonProperty("currentTrip")
    Trip currentTrip;
    @JsonProperty("driver")
    Driver driver;
    @JsonProperty("device")
    XvidiaDevice device;
    @JsonProperty("trips")
    List<Trip> trips;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerMobileNo() {
        return ownerMobileNo;
    }

    public void setOwnerMobileNo(String ownerMobileNo) {
        this.ownerMobileNo = ownerMobileNo;
    }

    public String getOwnerAddress() {
        return ownerAddress;
    }

    public void setOwnerAddress(String ownerAddress) {
        this.ownerAddress = ownerAddress;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public Boolean getOnTrip() {
        return isOnTrip;
    }

    public void setOnTrip(Boolean onTrip) {
        isOnTrip = onTrip;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Trip getCurrentTrip() {
        return currentTrip;
    }

    public void setCurrentTrip(Trip currentTrip) {
        this.currentTrip = currentTrip;
    }


    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public XvidiaDevice getDevice() {
        return device;
    }

    public void setDevice(XvidiaDevice device) {
        this.device = device;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

}
