package com.xvidia.cabi.backgroundService;

/*
 * Copyright (c) 2011 Dropbox, Inc.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Comparator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.mime.HttpMultipartMode;
import org.apache.mime.MultipartEntityBuilder;
import org.apache.mime.content.FileBody;
import org.apache.mime.content.StringBody;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.xvidia.cabi.data.LogData;
import com.xvidia.cabi.service.VideoRecorder;
import com.xvidia.cabi.utils.FileManager;
import com.xvidia.cabi.network.ServiceURLManager;



public class UploadOfflineVideoTask extends AsyncTask<Void, Void, String>{ 

    private String filePath;
//    private String fileName;
    private Context mContext;
//    Vector<UploadData> list;
    private String androidId;
    File[] sortedFileName;
    //private final ProgressDialog mDialog;

//    private String mErrorMsg;
    public UploadOfflineVideoTask(Context context){
    	mContext = context;
    	androidId = LogData.getInstance().getDeviceId(mContext);
    	sortedFileName = updateSortedFileList();
    	VideoRecorder.newDownload = true;
    }
		@Override
		protected String doInBackground(Void... params)  {
			boolean retVal = false;
//			UploadData obj = null;
//			for(int i= 0; i<list.size();i++){
//				obj= list.elementAt(i);
//				filePath  = obj.getFilePath();
//				boolean state = obj.getState();
//				if(checkNetwrk() && !filePath.isEmpty() && !state){	
//					obj.setUploadingState("true");
//					DataCacheManager.getInstance().updateOfflineUploadData(obj);
//				 retVal = executeMultipartPost();
//				 if(retVal){
//					File fileBody = new File(filePath);
//					fileName=fileBody.getName();
//					FileManager.deleteDir(fileBody);
//					DataCacheManager.getInstance().removeOfflineDataByName(fileName);
//				 }
//				}
//			}
			try{
			
				
					File temp;
					if(sortedFileName != null){
					for (int i = 0; i < sortedFileName.length; i++) {
						temp= sortedFileName[0];

						Log.d("UPLOAD MEDIA", "File name "+temp.getName()+" File position "+ i);
							if (temp.getName().contains(".mp4") && checkNetwrk()) {
								filePath = temp.getAbsolutePath();
								if(temp.length()>0)
								retVal = executeMultipartPost();
							}
							if(retVal){
								File fileBody = new File(filePath);
								FileManager.deleteDir(fileBody);
								sortedFileName = updateSortedFileList();
								if(sortedFileName == null){
									break;
								}
//							}else{
//								return "FAILED";
							}
					}
					}

					return "SUCCESS";
			}catch(Exception e){
				
			}
				return "SUCCESS";
			
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
//			 list = DataCacheManager.getInstance().getOfflineUploadDataList();
			
			
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
//			if(result.equals("SUCCESS")){
//				File fileBody = new File(filePath);
//				FileManager.deleteDir(fileBody);
//				DataCacheManager.getInstance().removePendingDataByName(fileName);
//			}else{
//				
//			}
			VideoRecorder.newDownload = false;
		}

		File[]  updateSortedFileList(){
			File[] sortedFileName = null;
			String destPath = Environment.getExternalStorageDirectory()+ VideoRecorder.FOLDER_PATH;
			File dir = new File (destPath);
				if (dir.isDirectory()) {
				 sortedFileName = dir.listFiles();
				if (sortedFileName != null && sortedFileName.length > 1) {
					Arrays.sort(sortedFileName, new Comparator<File>() {
			         @Override
			         public int compare(File object1, File object2) {
			        	 return object2.getName().compareTo(object1.getName());
			          }
			    });
			}
		}
				return sortedFileName;
		}
		public boolean executeMultipartPost(){
			boolean retFlag = false;
			try {

//				String eventId =AppSettingData.getInstance().getEvenId();
//				String uploadUri ="http://54.251.108.112:9080/uploadVideoMultiPart";
				String uploadUri = ServiceURLManager.getInstance().getUploadVideoUrl();;
//				String tripId = "Trip9";
//				androidId = "Device001";
				HttpClient httpClient = new DefaultHttpClient();

				Log.d("UPLOAD MEDIA", "Url Offline: "+ uploadUri);
				MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();        
				multipartEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

				ContentType cType = ContentType.TEXT_PLAIN;
				File fileBody = new File(filePath);
				
				Log.d("UPLOAD MEDIA", "File name "+fileBody.getName()+" File LEnth "+ fileBody.length());
				if(fileBody.length() == 0){
					return false;
				}
				HttpPost postRequest = new HttpPost(uploadUri);	
				multipartEntity.addPart("name",new StringBody(fileBody.getName(),cType));
				multipartEntity.addPart("deviceId",new StringBody(androidId,cType));
//				multipartEntity.addPart("tripId",new StringBody(tripId,cType));
				multipartEntity.addPart("file", new FileBody(fileBody/*,"application/octet"*/));


				HttpEntity entity = multipartEntity.build();
				postRequest.addHeader("Cache-Control", "no-cache");
				postRequest.setEntity(entity);

//				Log.d("UPLOAD MEDIA", "Url: "+ uploadUri);
				HttpResponse response = httpClient.execute(postRequest);
				String	returnVal = "";
				int responseCode = response.getStatusLine().getStatusCode();
				if(responseCode == 200){
					returnVal = EntityUtils.toString(response.getEntity(),"UTF-8");					
					retFlag = true;
				}else{
					returnVal = EntityUtils.toString(response.getEntity(),"UTF-8");	
					retFlag = false;
				}
				Log.e("UPLOAD MEDIA", "response: "+responseCode +" "+ returnVal);
//				Log.e("UPLOAD MEDIA", "response: "+responseCode +" "+ response.getStatusLine().getReasonPhrase());
			} 
			catch (UnsupportedEncodingException e) {
				Log.e("UPLOAD MEDIA", "ERRORRRRR: "+e.getMessage());
				retFlag = false;
			}
			catch (IOException e) {
				Log.e("UPLOAD MEDIA", "ERRORRRRR: "+e.getMessage());
				retFlag = false;
				}
			catch (ParseException e) {
				Log.e("UPLOAD MEDIA", "ERRORRRRR: "+e.getMessage());
				retFlag = false;
				}
			catch (Exception e) {
				Log.e("UPLOAD MEDIA", "ERRORRRRR: "+e.getMessage());
				retFlag = false;
			}

			return retFlag;
		}


	

//    @Override
//    protected void onProgressUpdate(Long... progress) {
//        int percent = (int)(100.0*(double)progress[0]/mFileLen + 0.5);
//        //mDialog.setProgress(percent);
//    }

		boolean checkNetwrk(){
			boolean nwFlag = false;
			try{		
				ConnectivityManager connMgr = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
				if (networkInfo != null && networkInfo.isConnected()) {
					nwFlag = true;
				}
			}catch (Exception e) {
				e.printStackTrace();
			}

			return nwFlag;
		}
//
//    private void showToast(String msg) {
//        Toast error = Toast.makeText(mContext, msg, Toast.LENGTH_LONG);
//        error.show();
//    }
}
