package com.xvidia.cabi.broadcastReceiver;

import java.lang.reflect.Method;
import java.util.Set;

import com.xvidia.obd.reader.io.ObdGatewayService;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

/**
 * This class extends {@link BroadcastReceiver} to start app as soon as boot is
 * completed
 * 
 * @author Ravi@xvidia
 * @since Version 1.0
 */
public class BluetoothReceiver extends BroadcastReceiver {

	static final String MY_PREFERENCES = "CABPREFERENCE";
	static final String BLUETOOTH_KEY = "BLUETOOTH_KEY";
	static Context appContext;
    private BluetoothAdapter myBluetoothAdapter;
    private Set<BluetoothDevice> pairedDevices;
    private BluetoothDevice dev = null;
	// private static final UUID MY_UUID =
	// UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	SharedPreferences sharedpreferences;

	@Override
	public void onReceive(Context context, Intent intent) {
		appContext = context;
		String action = intent.getAction();
		String[] devicenames = { "ELM", "ELM327", "CAN", "OBD", "OBD II", "OBDII"};
		if (BluetoothDevice.ACTION_FOUND.equals(action)) {
			// Get the BluetoothDevice object from the Intent
			BluetoothDevice device = intent
					.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			// add the name and the MAC address of the object to the
			// arrayAdapter
			String name = device.getName();
			Log.d("pairDevice()", "device.getName()" + name);
			if (name.equalsIgnoreCase(devicenames[0])
					|| name.contains(devicenames[0])
					|| name.equalsIgnoreCase(devicenames[1])
					|| name.contains(devicenames[1])
					|| name.equalsIgnoreCase(devicenames[2])
					|| name.contains(devicenames[2])
					|| name.equalsIgnoreCase(devicenames[3])
					|| name.contains(devicenames[3])
					|| name.equalsIgnoreCase(devicenames[4])
					|| name.contains(devicenames[4])
					|| name.equalsIgnoreCase(devicenames[5])
					|| name.contains(devicenames[5])) {
				pairDevice(device);
			}
		}else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
			BluetoothDevice device = intent
					.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			// add the name and the MAC address of the object to the
			// arrayAdapter
			String name = device.getName();
			Log.d("pairDevice()", "device.getName()" + name);
			if (name.equalsIgnoreCase(devicenames[0])
					|| name.contains(devicenames[0])
					|| name.equalsIgnoreCase(devicenames[1])
					|| name.contains(devicenames[1])
					|| name.equalsIgnoreCase(devicenames[2])
					|| name.contains(devicenames[2])
					|| name.equalsIgnoreCase(devicenames[3])
					|| name.contains(devicenames[3])
					|| name.equalsIgnoreCase(devicenames[4])
					|| name.contains(devicenames[4])
					|| name.equalsIgnoreCase(devicenames[5])
					|| name.contains(devicenames[5])) {
				pairDevice(device);
			}
		}
	}

	private void pairDevice(BluetoothDevice device) {
		try {
			// Log.d("pairDevice()", "Start Pairing...");
			Method m = device.getClass()
					.getMethod("createBond", (Class[]) null);
			m.invoke(device, (Object[]) null);
			setBluetoothKey(device.getAddress());
			  String remoteDevice = getSharedPreference().getString(BLUETOOTH_KEY, null);
		    	myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			  if(myBluetoothAdapter != null){
		        	if (!myBluetoothAdapter.isEnabled()) {
		          	  myBluetoothAdapter.enable();
		        	}
				dev = myBluetoothAdapter.getRemoteDevice(remoteDevice);
	    	    myBluetoothAdapter.cancelDiscovery();
    	    try {       	
    	    	ObdGatewayService.dev = dev;    	    	
    	        //
    	    	appContext.startService(new Intent(appContext,ObdGatewayService.class));
//    	    	 new Handler().postDelayed(new Runnable() {//        	        	 			
//    	 			@Override
//    	 			public void run() {
//    	 				Intent intent = new Intent(StartActivity.this, VideoRecorder.class);
//    	 				startActivity(intent);
//    	 				
//    	 			}
//    	 		}, 4000);
    	    	
    	      } catch (Exception e) {
    	        Log.e(
    	            "startObdConnection",
    	            "There was an error while establishing connection. -> "
    	                + e.getMessage()
    	        );
    	   }
			  }
			// Log.d("pairDevice()", "Pairing finished.");
		} catch (Exception e) {
			Log.e("pairDevice()", e.getMessage());
		}
	}

	private SharedPreferences getSharedPreference() {
		sharedpreferences = appContext.getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		return sharedpreferences;
	}

	public String getBluetoothKey() {
		String retVal = null;
		try {
			retVal = getSharedPreference().getString(BLUETOOTH_KEY, null);
			// retVal = readTextFromFile(FILE_NAME_ASSET);
		} catch (Exception e) {
		}
		return retVal;
	}

	public boolean setBluetoothKey(String prefVal) {
		boolean retVal = false;
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(BLUETOOTH_KEY, prefVal);
			editor.commit();
			retVal = true;
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
		return retVal;
	}
}