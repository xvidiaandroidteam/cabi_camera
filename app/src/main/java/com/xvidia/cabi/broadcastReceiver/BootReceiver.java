package com.xvidia.cabi.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.xvidia.cabi.service.StartActivity;

/**
 * This class extends {@link BroadcastReceiver} to start app as soon as boot is completed
 * @author Ravi@xvidia
 * @since Version 1.0
 */
public class BootReceiver extends BroadcastReceiver
{

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent myIntent = new Intent(context, StartActivity.class);
		myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(myIntent);
	}

}