package com.xvidia.obd.reader.io;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xvidia.cabi.data.LocationData;
import com.xvidia.cabi.data.LogData;
import com.xvidia.cabi.network.IAPIConstants;
import com.xvidia.cabi.network.ServiceURLManager;
import com.xvidia.cabi.network.VolleySingleton;
import com.xvidia.cabi.service.MyApplication;
import com.xvidia.obd.commands.ObdCommand;
import com.xvidia.obd.commands.SpeedObdCommand;
import com.xvidia.obd.commands.engine.EngineRPMObdCommand;
import com.xvidia.obd.commands.engine.EngineRuntimeObdCommand;
import com.xvidia.obd.commands.engine.ThrottlePositionObdCommand;
import com.xvidia.obd.commands.fuel.FindFuelTypeObdCommand;
import com.xvidia.obd.commands.pressure.FuelPressureObdCommand;
import com.xvidia.obd.commands.protocol.EchoOffObdCommand;
import com.xvidia.obd.commands.protocol.LineFeedOffObdCommand;
import com.xvidia.obd.commands.protocol.ObdResetCommand;
import com.xvidia.obd.commands.protocol.SelectProtocolObdCommand;
import com.xvidia.obd.commands.protocol.TimeoutObdCommand;
import com.xvidia.obd.commands.temperature.EngineCoolantTemperatureObdCommand;
import com.xvidia.obd.enums.AvailableCommandNames;
import com.xvidia.obd.enums.ObdProtocols;
import com.xvidia.obd.reader.config.ObdConfig;
import com.xvidia.obd.reader.data.ObdFuelData;
import com.xvidia.obd.reader.data.ObdPressureData;
import com.xvidia.obd.reader.data.ObdRpmData;
import com.xvidia.obd.reader.data.ObdSpeedData;
import com.xvidia.obd.reader.data.ObdTemperatureData;
import com.xvidia.obd.reader.data.ObdThrottleData;
import com.xvidia.obd.reader.io.ObdCommandJob.ObdCommandJobState;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

/**
 * This service is primarily responsible for establishing and maintaining a
 * permanent connection between the device where the application runs and a more
 * OBD Bluetooth interface.
 * <p/>
 * Secondarily, it will serve as a repository of ObdCommandJobs and at the same
 * time the application state-machine.
 */
public class ObdGatewayService extends AbstractGatewayService implements LocationListener {

	private static final String TAG = ObdGatewayService.class.getName();
  /*
   * http://developer.android.com/reference/android/bluetooth/BluetoothDevice.html
   * #createRfcommSocketToServiceRecord(java.util.UUID)
   *
   * "Hint: If you are connecting to a Bluetooth serial board then try using the
   * well-known SPP UUID 00001101-0000-1000-8000-00805F9B34FB. However if you
   * are connecting to an Android peer then please generate your own unique
   * UUID."
   */


	SharedPreferences sharedpreferences;
	public static final String MY_PREFERENCES = "CABPREFERENCE";
	public static final String BLUETOOTH_KEY = "BLUETOOTH_KEY";
	private static String SENSOR_ID = "sensorId";
	private static String DEVICE_ID = "deviceId";
	private static String TRIP_ID = "tripId";
	private static String REG_NO = "carRegNo";
	private static String TIMESTAMP = "timeStamp";
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
//  private final IBinder binder = new ObdGatewayServiceBinder();
//  SharedPreferences prefs;

	private LocationManager locationManager = null;
	private Criteria criteria;
	private String provider;
	static int TIMER_TIME_2SEC = 2 * 1000;
	public static String android_id;
	static String trip_id = "tripId";
	static String regNo = "regNo";
	private BluetoothAdapter myBluetoothAdapter;
	private Set<BluetoothDevice> pairedDevices;
	private ArrayList<String> BTArrayAdapter;
	static WebSocketClient mWebSocketClient;
	public static BluetoothDevice dev = null;
	private BluetoothSocket sock = null;
	boolean obdConnectionSuccess = false;

	//  private BluetoothSocket sockFallback = null;
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
//  public void startService() throws IOException {
		Log.d(TAG, "Starting service..");
//    final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
//	final List<RunningServiceInfo> services = activityManager
//			.getRunningServices(Integer.MAX_VALUE);
//	for (RunningServiceInfo runningServiceInfo : services) {
//		if (runningServiceInfo.service.getClassName().equals(
//				ObdGatewayService.class.getName())) {
//			try{
//				stopService();
//			}catch(Exception e){
//				
//			}
//		}
//	}  
//    prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		// get the remote Bluetooth device
//    final String remoteDevice = prefs.getString(ConfigActivity.BLUETOOTH_LIST_KEY, null);
//    if (remoteDevice == null || "".equals(remoteDevice)) {
//      Toast.makeText(ctx, "No Bluetooth device selected", Toast.LENGTH_LONG).show();
//
//      // log error
//      Log.e(TAG, "No Bluetooth device has been selected.");
//
//      stopService();
//      throw new IOException();
//      } else {
//
//    final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
//    dev = btAdapter.getRemoteDevice(remoteDevice);
//
//
//    /*
//     * Establish Bluetooth connection
//     *
//     * Because discovery is a heavyweight procedure for the Bluetooth adapter,
//     * this method should always be called before attempting to connect to a
//     * remote device with connect(). Discovery is not managed by the Activity,
//     * but is run as a system service, so an application should always call
//     * cancel discovery even if it did not directly request a discovery, just to
//     * be sure. If Bluetooth state is not STATE_ON, this API will return false.
//     *
//     * see
//     * http://developer.android.com/reference/android/bluetooth/BluetoothAdapter
//     * .html#cancelDiscovery()
//     */
//    Log.d(TAG, "Stopping Bluetooth discovery.");
//    btAdapter.cancelDiscovery();
//
//    showNotification("Tap to open OBD-Reader", "Starting OBD connection..", R.drawable.ic_launcher, true, true, false);

		try {
			setAndroidId();
//    	connectWebSocket();
			connectDeviceOverBlueTooth();
			initializeLocationManager();
//			new Handler().postDelayed(new Runnable() {
//				@Override
//				public void run() {
////				openLinkInChrome("com.xvidia.cabi.camera");
//				}
//			},2000);
		} catch (Exception e) {
			Log.e(
					TAG, "There was an error while establishing connection. -> "
							+ e.getMessage()
			);

			// in case of failure, stop this service.
			stopService();
//      throw new IOException();
		}

     /*
     * TODO clean
     *
     * Get more preferences
     */
//          boolean imperialUnits = prefs.getBoolean(ConfigActivity.IMPERIAL_UNITS_KEY,
//                  false);
//          ArrayList<ObdCommand> cmds = ConfigActivity.getObdCommands(prefs);
		return START_STICKY;
	}

	private void connectDeviceOverBlueTooth() {

		myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (myBluetoothAdapter != null) {
			if (!myBluetoothAdapter.isEnabled()) {
				myBluetoothAdapter.enable();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			BTArrayAdapter = new ArrayList<String>();
			String remoteDevice = getSharedPreference().getString(BLUETOOTH_KEY, null);
			String[] devicenames = {"ELM", "RAVI2", "ELM327", "OBD", "OBDII"};
			if (remoteDevice == null || "".equals(remoteDevice)) {
				pairedDevices = myBluetoothAdapter.getBondedDevices();
				BTArrayAdapter.clear();
				for (BluetoothDevice device : pairedDevices) {
//      		    	

					String name = device.getName();
					Log.d("pairDevice()", "device.getName()" + name);
					if (name.equalsIgnoreCase(devicenames[0])
							|| name.contains(devicenames[0])
							|| name.equalsIgnoreCase(devicenames[1])
							|| name.contains(devicenames[1])
							|| name.equalsIgnoreCase(devicenames[2])
							|| name.contains(devicenames[2])
							|| name.equalsIgnoreCase(devicenames[3])
							|| name.contains(devicenames[3])) {
						remoteDevice = device.getAddress();
						setBluetoothKey(remoteDevice);
						BTArrayAdapter.add(device.getName() + "\n" + device.getAddress());
					}
				}

				if (BTArrayAdapter.size() == 0) {
					BTArrayAdapter.clear();
					myBluetoothAdapter.startDiscovery();
//      				      Toast.makeText(getApplicationContext(),"Show Discovering Devices",Toast.LENGTH_SHORT).show();
					registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
					registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED));
					registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED));
					registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_PAIRING_REQUEST));

				} else {
					registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED));

					dev = myBluetoothAdapter.getRemoteDevice(remoteDevice);
					myBluetoothAdapter.cancelDiscovery();
					try {
//      	      	    	Intent serviceIntent = new Intent(this, ObdGatewayService.class);
						if (!obdConnectionSuccess)
							startObdConnection();
					} catch (Exception e) {
						Log.e(
								"startObdConnection",
								"There was an error while establishing connection. -> "
										+ e.getMessage()
						);
					}
				}

			} else {
				registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED));

				dev = myBluetoothAdapter.getRemoteDevice(remoteDevice);
				myBluetoothAdapter.cancelDiscovery();
				try {
//      	    	Intent serviceIntent = new Intent(this, ObdGatewayService.class);
					if (!obdConnectionSuccess)
						startObdConnection();
				} catch (Exception e) {
					Log.e(
							"startObdConnection",
							"There was an error while establishing connection. -> "
									+ e.getMessage()
					);
				}
			}
		}
	}


	final BroadcastReceiver bReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			// When discovery finds a device
			String[] devicenames = {"ELM", "RAVI2", "ELM327", "OBD", "OBDII"};
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				// Get the BluetoothDevice object from the Intent
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				// add the name and the MAC address of the object to the
				// arrayAdapter
				String name = device.getName();
				Log.d("pairDevice()", "device.getName()" + name);
				if (name.equalsIgnoreCase(devicenames[0])
						|| name.contains(devicenames[0])
						|| name.equalsIgnoreCase(devicenames[1])
						|| name.contains(devicenames[1])
						|| name.equalsIgnoreCase(devicenames[2])
						|| name.contains(devicenames[2])
						|| name.equalsIgnoreCase(devicenames[3])
						|| name.contains(devicenames[3])) {
					pairDevice(device);
				}
			} else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				// add the name and the MAC address of the object to the
				// arrayAdapter
				String name = device.getName();
				Log.d("pairDevice()", "device.getName()" + name);
				if (name.equalsIgnoreCase(devicenames[0])
						|| name.contains(devicenames[0])
						|| name.equalsIgnoreCase(devicenames[1])
						|| name.contains(devicenames[1])
						|| name.equalsIgnoreCase(devicenames[2])
						|| name.contains(devicenames[2])
						|| name.equalsIgnoreCase(devicenames[3])
						|| name.contains(devicenames[3])
						|| name.equalsIgnoreCase(devicenames[4])
						|| name.contains(devicenames[4])
						|| name.equalsIgnoreCase(devicenames[5])
						|| name.contains(devicenames[5])) {
					pairDevice(device);
				}
			} else if (BluetoothDevice.ACTION_PAIRING_REQUEST.equals(action)) {
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				// add the name and the MAC address of the object to the
				// arrayAdapter
				setBluetoothPairingPin(device);
				try {
					new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {

							try {
								if (!obdConnectionSuccess)
									startObdConnection();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					}, 3000);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	};
	void openLinkInChrome(String appPackageName) {
		try {
//			final String appPackageName = "com.bezuur.ui";//"xdk.intel.blank.ad.template.crosswalk";
//			Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(urlToOpen));
			Intent intent = getPackageManager().getLaunchIntentForPackage(appPackageName);
			if (intent != null) {

				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				intent.setClassName(appPackageName,"Main");
//				intent.addCategory("android.intent.category.LAUNCHER");
//				intent.setComponent(new ComponentName(appPackageName, "com.bezuur.ui.MainActivity"));
//				intent.setAction("android.intent.action.VIEW");
//				intent.addCategory("android.intent.category.BROWSABLE");
//                intent.setData(Uri.parse(urlToOpen));
				startActivity(intent);

			}
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
		e.printStackTrace();
		}

	}
	private void pairDevice(BluetoothDevice device) {
		try {
//	        Log.d("pairDevice()", "Start Pairing...");

//	        setBluetoothPairingPin(device);
			Method m = device.getClass().getMethod("createBond", (Class[]) null);
			m.invoke(device, (Object[]) null);
			try {
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {

						try {
							if (!obdConnectionSuccess)
								startObdConnection();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				}, 3000);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//	        setBluetoothKey(device.getAddress());
//	        Log.d("pairDevice()", "Pairing finished.");
		} catch (Exception e) {
			Log.e("pairDevice()", e.getMessage());
		}
	}

	public void setBluetoothPairingPin(BluetoothDevice device) {
		byte[] pinBytes = convertPinToBytes("1234");
		try {
			Log.d(TAG, "Try to set the PIN");
			Method m = device.getClass().getMethod("setPin", byte[].class);
			m.invoke(device, pinBytes);
			Log.d(TAG, "Success to add the PIN.");
			try {
				device.getClass().getMethod("setPairingConfirmation", boolean.class).invoke(device, true);
//	                device.getClass().getMethod("cancelPairingUserInput", boolean.class).invoke(device, true);
//	                byte[] pin = ByteBuffer.allocate(4).putInt(1234).array();
				Log.d(TAG, "Success to setPairingConfirmation.");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
	}

	private byte[] convertPinToBytes(String string) {
		byte[] bytes = null;
		try {
			bytes = string.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bytes;
	}

	/**
	 * Start and configure the connection to the OBD interface.
	 * <p/>
	 * See http://stackoverflow.com/questions/18657427/ioexception-read-failed-socket-might-closed-bluetooth-on-android-4-3/18786701#18786701
	 *
	 * @throws IOException
	 */
	private void startObdConnection() throws IOException {
		Log.d(TAG, "Starting OBD connection..");
		isRunning = true;

		obdConnectionSuccess = true;
		try {
			myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			String remoteDevice = getBluetoothKey();
			if (myBluetoothAdapter != null && dev == null) {
				dev = myBluetoothAdapter.getRemoteDevice(remoteDevice);
			}
			// Instantiate a BluetoothSocket for the remote device and connect it.
			sock = dev.createRfcommSocketToServiceRecord(MY_UUID);
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {

			}
			sock.connect();
		} catch (Exception e1) {
			Log.e(TAG, "There was an error while establishing Bluetooth connection. Falling back..", e1);
//      Class<?> clazz = sock.getRemoteDevice().getClass();
//      Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
			try {
				sock = (BluetoothSocket) dev.getClass().getMethod("createRfcommSocket", new Class[]{int.class}).invoke(dev, 1);
				sock.connect();

//        Method m = clazz.getMethod("createRfcommSocket", paramTypes);
//        Object[] params = new Object[]{Integer.valueOf(1)};
//        sockFallback = (BluetoothSocket) m.invoke(sock.getRemoteDevice(), params);
//        sockFallback.connect();
//        sock = sockFallback;
			} catch (Exception e2) {
				Log.e(TAG, "Couldn't fallback while establishing Bluetooth connection. Stopping app..", e2);
				obdConnectionSuccess = false;
//        stopService();
//        startObdConnection();
				throw new IOException();
			}
		}

		// Let's configure the connection.
//    Log.d(TAG, "Queing jobs for connection configuration..");
		queueJob(new ObdCommandJob(new ObdResetCommand()));
		queueJob(new ObdCommandJob(new EchoOffObdCommand()));

    /*
     * Will send second-time based on tests.
     *
     * TODO this can be done w/o having to queue jobs by just issuing
     * command.run(), command.getResult() and validate the result.
     */
		queueJob(new ObdCommandJob(new EchoOffObdCommand()));
		queueJob(new ObdCommandJob(new LineFeedOffObdCommand()));
		queueJob(new ObdCommandJob(new TimeoutObdCommand(62)));

		// Get protocol from preferences
//    String protocol = prefs.getString(ConfigActivity.PROTOCOLS_LIST_KEY,"AUTO");
		queueJob(new ObdCommandJob(new SelectProtocolObdCommand(ObdProtocols.valueOf("AUTO"))));

		// Job for returning dummy data
//    queueJob(new ObdCommandJob(new AmbientAirTemperatureObdCommand()));
//    for (ObdCommand Command : ObdConfig.getCommands()  ) {
////        if (prefs.getBoolean(Command.getName(),true))
//          queueJob(new ObdCommandJob(Command));
//         }
		queueCounter = 0L;
		Log.d(TAG, "Initialization jobs queued.");
		new Handler().postDelayed(mQueueCommands, 2000);

	}

	private final Runnable mQueueCommands = new Runnable() {
		public void run() {
			if (isRunning() && queueEmpty()) {
				queueCommands();
			}
			// run again in period defined in preferences
			new Handler().postDelayed(mQueueCommands, 3000);
		}
	};


	private void queueCommands() {
		for (ObdCommand Command : ObdConfig.getCommands()) {
			queueJob(new ObdCommandJob(Command));
		}

	}

	private SharedPreferences getSharedPreference() {
		sharedpreferences = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		return sharedpreferences;
	}

	public String getBluetoothKey() {
		String retVal = null;
		try {
			retVal = getSharedPreference().getString(BLUETOOTH_KEY, null);
			// retVal = readTextFromFile(FILE_NAME_ASSET);
		} catch (Exception e) {
		}
		return retVal;
	}

	public boolean setBluetoothKey(String prefVal) {
		boolean retVal = false;
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(BLUETOOTH_KEY, prefVal);
			editor.commit();
			retVal = true;
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
		return retVal;
	}

	/**
	 * Runs the queue until the service is stopped
	 */
	protected void executeQueue() throws InterruptedException {
		Log.d(TAG, "Executing queue..");
		while (!Thread.currentThread().isInterrupted()) {
			ObdCommandJob job = null;
			try {
				job = jobsQueue.take();

				// log job
				Log.d(TAG, "Taking job[" + job.getId() + "] from queue..");

				if (job.getState().equals(ObdCommandJobState.NEW)) {
//          Log.d(TAG, "Job state is NEW. Run it..");
					job.setState(ObdCommandJobState.RUNNING);
					job.getCommand().run(sock.getInputStream(), sock.getOutputStream());
				} else {

					// log not new job
//            Log.e(TAG,
//                "Job state was not new, so it shouldn't be in queue. BUG ALERT!");
				}
			} catch (InterruptedException i) {
				Thread.currentThread().interrupt();
			} catch (Exception e) {
				job.setState(ObdCommandJobState.EXECUTION_ERROR);
//        e.printStackTrace();
				if (e.getMessage().equalsIgnoreCase("Broken pipe")) {
//        	myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//        	if(myBluetoothAdapter != null){
//        		if(myBluetoothAdapter.isEnabled()){
//        			myBluetoothAdapter.disable();
//        			Thread.sleep(2000);
//        		}
//        	}
//        	 Log.e(TAG, "connect Device Over BlueTooth again -> ");
					stopService();
					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							Intent i = new Intent();
							i.setComponent(new ComponentName("com.xvidia.cabi", "com.xvidia.obd.reader.io.ObdGatewayService"));
							ComponentName c = startService(i);
							Log.e(TAG, "ComponentName restart. -> " + c);
						}
					},1000);
//					connectDeviceOverBlueTooth();
				}
				Log.e(TAG, "Failed to run command. -> " + e.getMessage());
			}

			if (job != null) {
				final ObdCommandJob job2 = job;
//        Log.e(TAG, "response to run command. -> " +job2.getCommand().getFormattedResult());
				final String cmdID = LookUpCommand(job2.getCommand().getName());
				updateTripStatistic(job2, cmdID);
//      ((MainActivity) ctx).runOnUiThread(new Runnable() {
//        @Override
//        public void run() {
//          ((MainActivity) ctx).stateUpdate(job2);
//        }
//      });
			}
		}
	}

	private void updateTripStatistic(final ObdCommandJob job, final String cmdID) {

		String result = "Sorry ";

		Date nw = new Date();
		long time = nw.getTime();
//	    if(currentTrip != null) {
		if (cmdID.equals(AvailableCommandNames.SPEED.toString())) {
			SpeedObdCommand command = (SpeedObdCommand) job.getCommand();
			result = "" + command.getMetricSpeed();
			float speed = (float) command.getMetricSpeed();
			sendSpeedRequest(time, speed);
		} else if (cmdID.equals(AvailableCommandNames.ENGINE_RPM.toString())) {
			EngineRPMObdCommand command = (EngineRPMObdCommand) job.getCommand();
			result = "" + command.getRPM();
			float rpm = (float) command.getRPM();
			sendRpmRequest(time, rpm);
		} else if (cmdID.endsWith(AvailableCommandNames.ENGINE_RUNTIME.toString())) {
			EngineRuntimeObdCommand command = (EngineRuntimeObdCommand) job.getCommand();
			result = "" + command.getFormattedResult();
		} else if (cmdID.endsWith(AvailableCommandNames.ENGINE_COOLANT_TEMP.toString())) {
			EngineCoolantTemperatureObdCommand command = (EngineCoolantTemperatureObdCommand) job.getCommand();
			result = "" + command.getFormattedResult();
			float temperature = (float) command.getTemperature();
			sendTemperatureRequest(time, temperature);
		} else if (cmdID.endsWith(AvailableCommandNames.THROTTLE_POS.toString())) {
			ThrottlePositionObdCommand command = (ThrottlePositionObdCommand) job.getCommand();
			result = "" + command.getFormattedResult();
			float percentage = (float) command.getPercentage();
			sendThrottleRequest(time, percentage);
		} else if (cmdID.endsWith(AvailableCommandNames.FUEL_PRESSURE.toString())) {
			FuelPressureObdCommand command = (FuelPressureObdCommand) job.getCommand();
			result = "" + command.getFormattedResult();
			float pressure = (float) command.getMetricUnit();
			sendPressureRequest(time, pressure);
		} else if (cmdID.endsWith(AvailableCommandNames.FUEL_TYPE.toString())) {
			FindFuelTypeObdCommand command = (FindFuelTypeObdCommand) job.getCommand();
			result = "" + command.getFormattedResult();
			String type = command.getFormattedResult();
			sendFuelTypeRequest(time, type);
		}
//	      Log.e(TAG, "response to run command. -> " +result);
	}

	public static String LookUpCommand(String txt) {
		for (AvailableCommandNames item : AvailableCommandNames.values()) {
			if (item.getValue().equals(txt)) return item.name();
		}
		return txt;
	}

	/**
	 * Stop OBD connection and queue processing.
	 */
	public void stopService() {
		Log.d(TAG, "Stopping service..");

//    notificationManager.cancel(NOTIFICATION_ID);
		jobsQueue.removeAll(jobsQueue); // TODO is this safe?
		isRunning = false;

		if (sock != null)
			// close socket
			try {
				sock.close();
			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
			}

		// kill service
		stopSelf();
	}

	public boolean isRunning() {
		return isRunning;
	}

	public class ObdGatewayServiceBinder extends Binder {
		public ObdGatewayService getService() {
			return ObdGatewayService.this;
		}
	}

	@Override
	public void startService() throws IOException {
	}

	private void connectWebSocket() {
		URI uri;
		try {
			if (mWebSocketClient != null) {
				mWebSocketClient.close();
			}
		} catch (Exception e) {
			Log.i("Websocket", "Onclose error");
		}
		mWebSocketClient = null;
		try {
//			uri = new URI("ws://54.251.255.172:9090/sensor");
        uri = new URI("ws://192.168.10.201:9999/sensor");
//        uri = new URI("ws://officecam.selfip.com:8080/sensor");
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return;
		}
		try {
			mWebSocketClient = new WebSocketClient(uri, new Draft_17()) {
				@Override
				public void onOpen(ServerHandshake serverHandshake) {
					Log.i("Websocket", "Opened");
					trip_id = "newTrip1";
					String carRegNo = LogData.getInstance().getCarRegNoKey(MyApplication.getAppContext());
					if (carRegNo == null) {
						carRegNo = "null";
					}
					regNo = carRegNo;
//                isConnected = true;
//                count = 1;

				}

				@Override
				public void onMessage(String s) {
					Log.i("Websocket", "onMessage Response " + s);
				}

				@Override
				public void onClose(int i, String s, boolean b) {
					Log.i("Websocket", "Closed " + s);
//                isConnected = false;
//                count = 0;
				}

				@Override
				public void onError(Exception e) {
					Log.e("Websocket", "Error " + e.getMessage());
				}


			};
			mWebSocketClient.connect();
		} catch (Exception e) {
			Log.e("Websocket", "Error " + e.getMessage());
		}
	}

	/**
	 * In this method Location services is initialised
	 */
	private void initializeLocationManager() {
		// Get the location manager
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		// Define the criteria how to select the location provider
		criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_COARSE);    //default

		criteria.setCostAllowed(false);
		// get the best provider depending on the criteria
		provider = locationManager.getBestProvider(criteria, false);

		// the last known location of this provider
		Location location = locationManager.getLastKnownLocation(provider);


		if (location != null) {
			onLocationChanged(location);
		} else {
			// leads to the settings because there is no last known location
//		  Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//		  startActivity(intent);
		}
		locationManager.requestLocationUpdates(provider, TIMER_TIME_2SEC, 1, this);
		// location updates: at least 1 meter and 200millsecs change
	}


	@Override
	public void onLocationChanged(Location location) {
		float lat = (float) location.getLatitude();
		float longitude = (float) location.getLongitude();
		float altitude = (float) location.getAltitude();
		float speed = (float) location.getSpeed();
		Date nw = new Date();
		long time = nw.getTime();


		Log.i("onLocation Service", "onLocationChanged " + speed);

		sendLocationRequest(time, lat, longitude, altitude, speed);

	}

	@Override
	public void onProviderDisabled(String arg0) {

	}

	@Override
	public void onProviderEnabled(String arg0) {

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

	}


	/**
	 * Sends Location data
	 *
	 * @param timeStamp
	 * @param latitude
	 * @param longitude
	 * @param altitude
	 * @param speed
	 */
	public void sendLocationRequest(long timeStamp, float latitude, float longitude, float altitude, float speed) {
		if (android_id == null) {
			setAndroidId();
		}else if(android_id.isEmpty()){
			setAndroidId();
		}
		JSONObject jsonRequest = null;
		ObjectMapper mapper = new ObjectMapper();
		String jsonRequestString = null;
		LocationData locationData = new LocationData();
		locationData.setSensorId("GPS");
		locationData.setId(""+timeStamp);
		locationData.setDeviceId(android_id);
		locationData.setCarRegNo(regNo);
		locationData.setTripId(trip_id);
		locationData.setLat(latitude);
		locationData.setLongitude(longitude);
		locationData.setAltitude(altitude);
		locationData.setSpeed(speed);
		locationData.setTimeStamp(timeStamp);
		try {
			jsonRequestString = mapper.writeValueAsString(locationData);
			Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
		} catch (IOException e) {
		}

		sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_LOCATION);
	}

	/**
	 * Checks internet connectivity
	 *
	 * @return boolean true/false
	 */

	boolean checkNetwrk() {
		boolean nwFlag = false;
		try {
			ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isConnected()) {
				nwFlag = true;
			}

		} catch (Exception e) {
		}

		return nwFlag;
	}

	/**
	 * Sends http request to server
	 *
	 * @param jsonRequest
	 * @param api
	 */
	private void sendPostRequest(String jsonRequest, int api) {
		if (jsonRequest != null) {

			if (checkNetwrk()) {
				final String url = ServiceURLManager.getInstance().getUrl(api);

				JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						try {
							Log.i("Volley JSONObject ", response.toString());
						} catch (Exception e) {

						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						try {
							if (error != null) {
								NetworkResponse networkResponse = error.networkResponse;
								Log.i("Volley error service ", error.getMessage());
							}
						}catch (Exception e){

						}
					}
				});
				VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
			}
		}
	}

	/**
	 * sends Speed data to server
	 *
	 * @param timeStamp
	 * @param speed
	 */
	public void sendSpeedRequest(long timeStamp, float speed) {
		if (android_id == null) {
			setAndroidId();
		}else if(android_id.isEmpty()){
			setAndroidId();
		}
		JSONObject jsonRequest = null;
		ObjectMapper mapper = new ObjectMapper();
		String jsonRequestString = null;
		ObdSpeedData obdSpeedData = new ObdSpeedData();
		obdSpeedData.setDeviceId(android_id);
		obdSpeedData.setCarRegNo(regNo);
		obdSpeedData.setTripId(trip_id);
		obdSpeedData.setId("" + timeStamp);
		obdSpeedData.setSpeed(speed);
		obdSpeedData.setTimeStamp(timeStamp);
		try {
			jsonRequestString = mapper.writeValueAsString(obdSpeedData);
			Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
		} catch (IOException e) {
		}

		sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_OBD_SPEED);
	}

	/**
	 * Sends engine coolant temperature to server
	 *
	 * @param timeStamp
	 * @param temperature
	 */
	public void sendTemperatureRequest(long timeStamp, float temperature) {
		if (android_id == null) {
			setAndroidId();
		}else if(android_id.isEmpty()){
			setAndroidId();
		}
		JSONObject jsonRequest = null;
		ObjectMapper mapper = new ObjectMapper();
		String jsonRequestString = null;
		ObdTemperatureData obdTemperatureData = new ObdTemperatureData();
		obdTemperatureData.setDeviceId(android_id);
		obdTemperatureData.setCarRegNo(regNo);
		obdTemperatureData.setTripId(trip_id);
		obdTemperatureData.setId("" + timeStamp);
		obdTemperatureData.setCoolantTemperature(temperature);
		obdTemperatureData.setTimeStamp(timeStamp);
		try {
			jsonRequestString = mapper.writeValueAsString(obdTemperatureData);
			Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
		} catch (IOException e) {
		}

		sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_OBD_COOLANT_TEMPERATURE);
	}

	/**
	 * Send Engine Throttle data to server
	 *
	 * @param timeStamp
	 * @param throttle
	 */
	public void sendThrottleRequest(long timeStamp, float throttle) {
		if (android_id == null) {
			setAndroidId();
		}else if(android_id.isEmpty()){
			setAndroidId();
		}
		JSONObject jsonRequest = null;
		ObjectMapper mapper = new ObjectMapper();
		String jsonRequestString = null;
		ObdThrottleData obdThrottleData = new ObdThrottleData();
		obdThrottleData.setDeviceId(android_id);
		obdThrottleData.setCarRegNo(regNo);
		obdThrottleData.setTripId(trip_id);
		obdThrottleData.setId("" + timeStamp);
		obdThrottleData.setThrottle(throttle);
		obdThrottleData.setTimeStamp(timeStamp);
		try {
			jsonRequestString = mapper.writeValueAsString(obdThrottleData);
			Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
		} catch (IOException e) {
		}

		sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_OBD_THROTTLE);
	}

	/**
	 * Sends RPM data to server
	 *
	 * @param timeStamp
	 * @param rpm
	 */
	public void sendRpmRequest(long timeStamp, float rpm) {
		if (android_id == null) {
			setAndroidId();
		}else if(android_id.isEmpty()){
			setAndroidId();
		}
		JSONObject jsonRequest = null;
		ObjectMapper mapper = new ObjectMapper();
		String jsonRequestString = null;
		ObdRpmData obdRpmData = new ObdRpmData();
		obdRpmData.setDeviceId(android_id);
		obdRpmData.setCarRegNo(regNo);
		obdRpmData.setTripId(trip_id);
		obdRpmData.setId("" + timeStamp);
		obdRpmData.setRpm(rpm);
		obdRpmData.setTimeStamp(timeStamp);
		try {
			jsonRequestString = mapper.writeValueAsString(obdRpmData);
			Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
		} catch (IOException e) {
		}

		sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_OBD_RPM);
	}

	/**
	 * Sends fuel type information to server
	 *
	 * @param timeStamp
	 * @param fuelType
	 */
	public void sendFuelTypeRequest(long timeStamp, String fuelType) {
		if (android_id == null) {
			setAndroidId();
		}else if(android_id.isEmpty()){
			setAndroidId();
		}
		JSONObject jsonRequest = null;
		ObjectMapper mapper = new ObjectMapper();
		String jsonRequestString = null;
		ObdFuelData obdFuelData = new ObdFuelData();
		obdFuelData.setDeviceId(android_id);
		obdFuelData.setCarRegNo(regNo);
		obdFuelData.setTripId(trip_id);
		obdFuelData.setFuelType(fuelType);
		obdFuelData.setId("" + timeStamp);
		obdFuelData.setTimeStamp(timeStamp);
		try {
			jsonRequestString = mapper.writeValueAsString(obdFuelData);
			Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
		} catch (IOException e) {
		}

		sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_OBD_FUELTYPE);
	}

	/**
	 * Sends fuel pressure information to server
	 *
	 * @param timeStamp
	 * @param fuelPressure
	 */
	public void sendPressureRequest(long timeStamp, float fuelPressure) {
		if (android_id == null) {
			setAndroidId();
		}else if(android_id.isEmpty()){
			setAndroidId();
		}
		JSONObject jsonRequest = null;
		ObjectMapper mapper = new ObjectMapper();
		String jsonRequestString = null;
		ObdPressureData obdPressureData = new ObdPressureData();
		obdPressureData.setDeviceId(android_id);
		obdPressureData.setCarRegNo(regNo);
		obdPressureData.setTripId(trip_id);
		obdPressureData.setFuelPressure(fuelPressure);
		obdPressureData.setId("" + timeStamp);
		obdPressureData.setTimeStamp(timeStamp);
		try {
			jsonRequestString = mapper.writeValueAsString(obdPressureData);
			Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
		} catch (IOException e) {
		}

		sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_OBD_PRESSURE);
	}

	private void setAndroidId() {
		android_id = LogData.getInstance().getDeviceId(MyApplication.getAppContext());//Secure.getString(getApplicationContext().getContentResolver(),Secure.ANDROID_ID);
//	android_id = "Cam1";

	}

	public static void sendMessage(String message) {
		try {
			mWebSocketClient.send(message);
			Log.i("mWebSocketClient", "Wsclient message " + message);
		} catch (IllegalStateException e) {
			Log.e("mWebSocketClient", "Wsclient error message " + e.getMessage());
		} catch (Exception e) {
			Log.e("mWebSocketClient", "Wsclient error message " + e.getMessage());
		}
	}
}